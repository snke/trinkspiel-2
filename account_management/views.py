from django.shortcuts import render
from django_registration.backends.activation.views import RegistrationView
from .forms.registrationform import registrationForm
from django_registration.signals import user_registered

# Create your views here.

class registration(RegistrationView):

    form_class = registrationForm

    def get_success_url(self, user=None):
        """
        Return the URL to redirect to after successful redirection.
        """
        # This is overridden solely to allow django-registration to
        # support passing the user account as an argument; otherwise,
        # the base FormMixin implementation, which accepts no
        # arguments, could be called and end up raising a TypeError.
        return super(registration, self).get_success_url()

    def register(self,form):
        new_user = self.create_inactive_user(form)
        user_registered.send(
            sender=self.__class__,
            user=new_user,
            request=self.request
        )
        return new_user
