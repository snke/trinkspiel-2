var tempHeartColor;

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

$(".follow").click(function() {
    button = $(this)
    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
            return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
        }
        var csrftoken = getCookie('csrftoken');
        $.ajax({
            method: "POST",
            dataType:"json",
            url:"../../../follow/",
            beforeSend: function(xhr, settings) {
                if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                    xhr.setRequestHeader("X-CSRFToken", csrftoken);
                }
            },
            data: {id: $(this).attr("id")},
            success: function(result) {
                    status = result.status;
                    if(status == 200){
                        if(result.content.result){
                           button.html("Folgen");
                           button.attr("class", "btn btn-primary text-white follow");
                        }
                        else {
                            button.attr("class", "btn btn-secondary text-white follow");
                            button.html("Entfolgen");
                        }
                    }
                    else {
                        console.log(result);
                    }
                }
        });
});

$(".heart").click(function() {
    heart = $(this)
    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
            return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
        }
        var csrftoken = getCookie('csrftoken');
        $.ajax({
            method: "POST",
            dataType:"json",
            url:"../../../like/",
            beforeSend: function(xhr, settings) {
                if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                    xhr.setRequestHeader("X-CSRFToken", csrftoken);
                }
            },
            data: {id: $(this).attr("id")},
            success: function(result) {
                    console.log(result);
                    status = result.status;
                    if(status == 200){
                        if(result.content.result){
                            tempHeartColor = "white";
                            heart.children().attr("fill", "white");
                            heart.children().attr("stroke", "black");
                        }
                        else {
                            tempHeartColor = "#FC4949";
                            heart.children().attr("fill", "#FC4949");
                            heart.children().attr("stroke", "#FC4949");
                        }
                    }
                    else {
                        console.log("error");
                    }
                }
        });
});

$(".heart").hover(function() {
    tempHeartColor = $(this).children().attr("fill");
    $(this).children().attr("fill", "#FF9797");
    $(this).children().attr("stroke", "#FF9797");
}, function() {
    $(this).children().attr("fill", tempHeartColor);
    if ($(this).children().attr("fill") == "#FC4949") {
        $(this).children().attr("stroke", "#FC4949");
    } else {
        $(this).children().attr("stroke", "black");
    }

});