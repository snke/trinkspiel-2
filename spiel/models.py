from django.db import models
from django.utils.timezone import now
from django.contrib.auth.models import User


class Stapel(models.Model):
    Owner = models.ForeignKey(User, on_delete=models.CASCADE)
    Name = models.CharField(max_length=50, null=False, default="Kartenset")
    DefaultSet = models.BooleanField(default=False)
    Shared = models.BooleanField(default=False)
    Created = models.DateTimeField(default=now)
    CountFollowedBy = models.IntegerField(default=0)
    CountLikedBy = models.IntegerField(default=0)
    FollowedBy = models.ManyToManyField(User, related_name="followUser", blank=True)
    LikedBy = models.ManyToManyField(User, related_name="likedUser", blank=True)
    Played = models.IntegerField(default=0)

    def __str__(self):
        return self.Name


class Karte(models.Model):
    Task = models.CharField(max_length=500)
    TaskAnswer = models.CharField(max_length=500, null=True, blank=True)
    Category = models.PositiveSmallIntegerField()
    Stapel = models.ForeignKey(Stapel, on_delete=models.CASCADE)


class Kommentar(models.Model):
    Ersteller = models.ForeignKey(User, on_delete=models.CASCADE)
    Timestamp = models.DateTimeField(auto_now_add=True)
    Kommentar = models.CharField(max_length=1500, null=False)
    Titel = models.CharField(max_length=50, null=False)
    Freigeschaltet = models.BooleanField(null=False, default=True)

    def __str__(self):
        return self.Titel


class CaptchaRequired(models.Model):
    """
    Leo: Dieses Model ist für dich relevant. Beim Login muss in der View geprüft werden, ob der Nutzer ein
    Captcha ausfüllen muss. Dies wird in diesem Model gespeichert.
    """
    User = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    Captcha = models.BooleanField(default=False)
