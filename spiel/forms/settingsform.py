from django import forms
from django.contrib.auth import models
import django.core.exceptions as exceptions

class SettingUsernameForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput, label="Passwort zum bestätigen:")

    class Meta:
        model = models.User
        fields = ['username']


class SettingEmailForm(forms.Form):
    password = forms.CharField(widget=forms.PasswordInput, label="Passwort zum bestätigen:")
    email = forms.EmailField(label="Neue Email-Adresse:")


class SettingPasswordForm(forms.Form):
    old_password = forms.CharField(widget=forms.PasswordInput, label="Altes Passwort:")
    password = forms.CharField(widget=forms.PasswordInput, label="Neues Passwort:")
    password2 = forms.CharField(widget=forms.PasswordInput, label="Passwort wiederholen:")

    def clean_password2(self):
        if self.cleaned_data['password2'] != self.cleaned_data['password']:
            raise exceptions.ValidationError("Die Passwörter stimmen nicht überein.")
