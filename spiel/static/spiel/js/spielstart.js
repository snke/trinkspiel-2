$("#addPlayer").click(function() {
    $('<div class="form-group" style="display: none;"><input type="text" class="form-control playername" id="name" name="name" placeholder="Name"></div>').appendTo($('#names')).slideDown("fast");
    $( ".playername" ).blur(function() {
        if(!$(this).val()){
            $(this).slideUp("fast", function() {
               $(this).remove();
            });
        }
    });
});