from django_registration.forms import RegistrationFormTermsOfService
from antispam.captcha.forms import ReCAPTCHA

class registrationForm(RegistrationFormTermsOfService):
    captcha = ReCAPTCHA()
