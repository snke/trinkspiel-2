from django import forms
from spiel.models import Stapel, Karte
from crispy_forms.helper import FormHelper


class KartenForm(forms.ModelForm):
    class Meta:
        model = Karte
        fields = ('Task', 'TaskAnswer', 'Category')
        widgets = {'Task': forms.Textarea(attrs={'cols': None, 'rows': None, 'class': 'cardtask'}),
                   'TaskAnswer': forms.Textarea(attrs={'cols': None, 'rows': None}),
                   'Category': forms.Select(choices=(('1','Aufgabe'),('2','Regel'),('3','Spiel')))}

    def __init__(self, *args, **kwargs):
        super(KartenForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_show_labels = False

#TODO: Customverification hinzufügen. Folgende Punkte:
# 1. Wenn Task leer ist, wird element gelöscht  --> So halb gelöst
# 2. Error Handling implemenmtieren
# 3. Code dokumentieren