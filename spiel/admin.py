from django.contrib import admin
from .models import Stapel, Karte, Kommentar

# Register your models here.
class KarteInline(admin.StackedInline):
    model = Karte
    extra = 3


class StapelAdmin(admin.ModelAdmin):
    inlines = [KarteInline]


admin.site.register(Stapel, StapelAdmin)
admin.site.register(Kommentar)
