from django.urls import  path
from django.views.generic.base import RedirectView
from . import views, ajax_views

urlpatterns = [
    path(r'', RedirectView.as_view(pattern_name='gamestart'), name="index"),
    path(r'start/', views.start, name="gamestart"),
    path(r'fetch/', ajax_views.fetch, name="fetch"),
    path(r'dashboard/own/', views.ownDashboard, name="ownsets"),
    path(r'dashboard/follow/', views.followDashboard, name="followsets"),
    path(r'create/', views.create, name="create"),
    path(r'shared/', ajax_views.shared, name="shared"),
    path(r'deckname/', ajax_views.deckname, name="deckname"),
    path(r'deletedeck/', ajax_views.deletedeck, name="deletedeck"),
    path(r'discover/', views.discover, {"mode":"normal"}, name="discover"),
    path(r'discover/hot/', views.discover, {"mode":"hot"}),
    path(r'discover/random/', views.discover, {"mode":"random"}),
    path(r'discover/mostplayed/', views.discover, {"mode":"played"}),
    path(r'discover/mostliked/', views.discover, {"mode":"liked"}),
    path(r'discover/mostfollowed/', views.discover, {"mode":"followed"}),
    path(r'follow/', ajax_views.follow, name="follow"),
    path(r'like/', ajax_views.like, name="like"),
    path(r'comment/', views.kommentare, name="comment"),
    path(r'settings/', views.profil_settings, name="settings")
]