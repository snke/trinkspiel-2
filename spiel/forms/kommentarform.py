from django import forms
from spiel.models import Kommentar
from antispam.honeypot.forms import HoneypotField


class KommentarForm(forms.ModelForm):
    Datenschutz = forms.BooleanField(label="Ich habe die <a href='#'>Datenschutzerklärung</a> gelesen und bin damit einverstanden.")
    email = HoneypotField()

    class Meta:
        model = Kommentar
        fields = ['Titel','Kommentar','Ersteller']
        exclude = ['Ersteller']
        widgets = {
            'Kommentar': forms.Textarea(attrs={'cols': 40, 'rows': 10}),
        }