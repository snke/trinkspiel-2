$( document ).ready(function () {
    $('select:last').prop('selectedIndex', -1);
    register();
    activateCategoryDeactivator();
    task();
});


function activateCategoryDeactivator(){
    $('.select').change(function() {
                    var sel = $(this).val();
                    var id = $(this).attr('id').replace('id_karte_set-', '').replace('-Category', '');
                    if (sel == '1' || sel == '3') {
                        $('textarea[name="karte_set-'+id+'-TaskAnswer"]').prop({disabled: true});
                        $('textarea[name="karte_set-'+id+'-TaskAnswer"]').prop({required: false});
                    }
                    else {
                        $('textarea[name="karte_set-'+id+'-TaskAnswer"]').prop({disabled: false});
                        $('textarea[name="karte_set-'+id+'-TaskAnswer"]').prop({required: true});
                    }
                });
}


function task() {
    $('.cardtask').change(function(){
        var id = $(this).attr('id').replace('id_karte_set-', '').replace('-Task', '');
        console.log(id);
        if ($(this).val() == ""){
            $('#id_karte_set-'+id+'-DELETE').prop('checked', true);
            $('#id_karte_set-'+id+'-Category').prop('selectedIndex', -1);
            $('#id_karte_set-'+id+'-TaskAnswer').prop({disabled: true});
        }
        else {
            $('#id_karte_set-'+id+'-DELETE').prop('checked', false);
            $('#id_karte_set-'+id+'-Category').prop('selectedIndex', 0);
            ('#id_karte_set-'+id+'-TaskAnswer').prop({disabled: false});
        }
    });
}

function register() {
    $('.cardtask:last').focus(function() {
        var newCard = $('.createcard:last').clone(false);
        var total = $('#id_karte_set-TOTAL_FORMS').val();

        newCard.find('textarea').each(function() {
            var name = $(this).attr('name').replace('-' + (total-1) + '-','-' + total + '-');
            var id = 'id_' + name;
            $(this).attr({'name': name, 'id': id}).val('').removeAttr('required');
        });

        newCard.find('select').each(function() {
            var name = $(this).attr('name').replace('-' + (total-1) + '-','-' + total + '-');
            var id = 'id_' + name;
            $(this).attr({'name': name, 'id': id}).val('').removeAttr('checked');
        });

        newCard.find('input').each(function() {
            var name = $(this).attr('name').replace('-' + (total-1) + '-','-' + total + '-');
            var id = 'id_' + name;
            $(this).attr({'name': name, 'id': id}).val('').removeAttr('checked');
        });

         newCard.find('label').each(function() {
            var newFor = $(this).attr('for').replace('-' + (total-1) + '-','-' + total + '-');
            $(this).attr('for', newFor);
        });

        total++;
        $('#id_karte_set-TOTAL_FORMS').val(total);
        $('.all_cards').append(newCard);
        $(this).off();
        register();
        $('.select').off();
        activateCategoryDeactivator();
        task();
    });
}