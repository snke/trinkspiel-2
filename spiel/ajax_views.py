from django_ajax.decorators import ajax
from django.forms.models import model_to_dict
from django.http import HttpResponseBadRequest, HttpResponse
from .models import Stapel, Karte
from django.core.exceptions import ObjectDoesNotExist


@ajax
def shared(request):
    """
    Ajaxhandler, welcher den Status einer Karte anpasst, ob diese öffentlich oder privat ist
    TODO: Exceptionhandling implementieren
    :param request:
    :return:
    """
    try:
        deck = Stapel.objects.get(id=request.GET.get('id'), Owner=request.user)
        deck.Shared = request.GET.get('status')
        deck.save()
    except:
        return HttpResponseBadRequest
    return


@ajax
def fetch(request):
    """
    Ajaxhandler, welcher betsimmte Karten holt
    TODO: Exceptionhandling implementieren
    :param request:
    :return:
    """
    try:
        task = Karte.objects.get(id=request.GET['id'], Stapel__Shared=True)
    except ObjectDoesNotExist:
        try:
            task = Karte.objects.get(id=request.GET['id'], Stapel__Owner=request.user)
        except ObjectDoesNotExist:
            return {'id':0, 'Task':'Karte existiert nicht!', 'TaskAnswer': '', 'Category':0, 'Stapel':1}
    return model_to_dict(task)

@ajax
def deckname(request):
    """
    Ajaxhandler, welcher den Stapelnamen ändert
    TODO: implement error handling wenn meht als 50 Zeichen
    :param request:
    :return:
    """
    try:
        stapel = Stapel.objects.get(id=request.GET.get('id'))
        stapel.Name = request.GET.get('name')
        stapel.full_clean()
        stapel.save()
        return
    except:
        return HttpResponseBadRequest()


def deletedeck(request):
    """
    Ajaxhandler, welcher den Stapel löscht
    :param request:
    :return:
    """
    try:
        stapel = Stapel.objects.get(id=request.GET.get('id'))
        if stapel.Owner == request.user:
            stapel.delete()
            response = HttpResponse()
            response.status_code = 200
            return response
        else:
            return HttpResponseBadRequest()
    except:
        return HttpResponseBadRequest()

@ajax
def follow(request):
    """
    Ajaxhandler, um Stapel zu folgen
    :param request:
    :return:
    """
    try:
        stapel = Stapel.objects.get(id=request.POST.get('id'))
        if stapel.Shared == 1:
            if request.user in stapel.FollowedBy.all():
                stapel.CountFollowedBy = stapel.CountFollowedBy - 1
                stapel.save()
                stapel.FollowedBy.remove(request.user)
                return {"result": True}
            else:
                stapel.CountFollowedBy = stapel.CountFollowedBy + 1
                stapel.save()
                stapel.FollowedBy.add(request.user)
                return {"result": False}
        else:
            return HttpResponseBadRequest()
    except:
        return HttpResponseBadRequest()

@ajax
def like(request):
    """
    Ajaxhandler, um Stapel zu folgen
    :param request:
    :return:
    """
    try:
        stapel = Stapel.objects.get(id=request.POST.get('id'))
        if stapel.Shared == 1:
            if request.user in stapel.LikedBy.all():
                stapel.CountLikedBy = stapel.CountLikedBy - 1
                stapel.save()
                stapel.LikedBy.remove(request.user)
                return {"result": True}
            else:
                stapel.LikedBy.add(request.user)
                stapel.CountLikedBy = stapel.CountLikedBy + 1
                stapel.save()
                return {"result": False}
        else:
            return HttpResponseBadRequest()
    except:
        return HttpResponseBadRequest()