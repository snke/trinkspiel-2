Hallo {{user}},

Aktiviere mit dem folgenden Link deinen Account inerhalb von {{expiration_days}} und fange direkt an zu spielen.

http://localhost:8000/accounts/activate/{{activation_key}}

Viel Spaß und guten Durst