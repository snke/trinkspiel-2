from django.urls import  path
from . import views

urlpatterns = [
    path(r'', views.registration.as_view(), name="register"),
]