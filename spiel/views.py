from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseBadRequest

from django.core.paginator import Paginator
from django.db import transaction
from django.db.models import Count, F
from django.utils.safestring import mark_safe
from .models import Stapel, Karte, Kommentar
from django.contrib.auth.decorators import login_required
import urlextract
from .forms.createform import KartenForm
from .forms.settingsform import SettingUsernameForm, SettingEmailForm, SettingPasswordForm
from django.forms import inlineformset_factory
from django.contrib.auth.models import User


#Exceptions
from django.core import exceptions

#Time packages
from django.utils import timezone
from datetime import timedelta

#Forms
from .forms.kommentarform import KommentarForm

#TODO: Initial site von spielsite trennen
def start(request):
    defaultSets = Stapel.objects.all().filter(DefaultSet=True)
    args = {"defaultSets": defaultSets}
    if request.user.is_authenticated:
        ownSets = Stapel.objects.all().filter(Owner=request.user).annotate(cards=Count('karte'))
        ownSets = ownSets.all().filter(cards__gt=0)
        followedSets = Stapel.objects.all().filter(FollowedBy=request.user, Shared=True).exclude(Owner=request.user)
        args.update({"ownSets": ownSets, "followedSets": followedSets})
    if request.method == "POST" and len(list(filter(None, request.POST.getlist("name")))) >= 2 and request.POST.getlist("stapelCheckBox"):
        allCards = []
        ids = []
        for element in request.POST.getlist("stapelCheckBox"):
            if Stapel.objects.get(id=element).Shared:
                stapel = Stapel.objects.get(id=element)
                stapel.Played = stapel.Played + 1
                stapel.save()
                allCards.extend(list(Karte.objects.filter(Stapel_id=element)))
            elif Stapel.objects.get(id=element).Owner == request.user:
                stapel = Stapel.objects.get(id=element)
                stapel.Played = stapel.Played + 1
                stapel.save()
                allCards.extend(list(Karte.objects.filter(Stapel_id=element)))
        for element in allCards:
           ids.append(element.id)
        return render(request, 'spiel/spiel.html', {'ids': ids, 'names': mark_safe(list(filter(None,request.POST.getlist("name"))))})
    elif request.method == "POST":
        args = {"defaultSets": defaultSets, "error": True, "names": list(filter(None, request.POST.getlist("name"))), "selected": request.POST.getlist("stapelCheckBox")}
        return render(request, 'spiel/spielstart.html', args)
    return render(request, 'spiel/spielstart.html', args)


#TODO: Auf Django From umschreiben
@login_required
def ownDashboard(request):
    if request.method == "POST":
        if request.POST.get('stapelName') != None:
            try:
                stapel = Stapel(Name=request.POST.get('stapelName'), Owner=request.user)
                stapel.full_clean()
                stapel.save()
                return redirect('../../create/?id='+str(stapel.id))
            except:
                pass
    Sets = Stapel.objects.all().filter(Owner=request.user)
    mySets = Sets.annotate(number_cards=Count('karte'))
    args = {"elements": mySets}
    return render(request, 'spiel/ownSets.html', args)


#TODO: error handling
@login_required()
def followDashboard(request):
    try:
        Sets = stapel = Stapel.objects.all().filter(DefaultSet=0, Shared=1, FollowedBy=request.user)
        mySets = Sets.annotate(number_cards=Count('karte'))
        args = {"elements": mySets}
    except:
        pass
    return render(request, 'spiel/follow.html', args)


#TODO: Refactor den Code irgendwann mal, wenn du Django Forms verstanden hast
@login_required
def create(request):
    """
    GET: Gibt die Seite zum erstellen von Karten zurück
    POST: Verarbeitet das erstellen der neuen Stapel und prüft auf Richtigkeit
    :param request:
    :return:
    """
    try:
        stapel = Stapel.objects.get(id=request.GET.get('id'), Owner=request.user)
    except exceptions.ObjectDoesNotExist:
        return redirect('ownsets')

    KarteFormSet = inlineformset_factory(Stapel, Karte, extra=1, form=KartenForm)
    if request.method == "POST":
        formset = KarteFormSet(request.POST, instance=stapel)
        if formset.is_valid():
            print(formset.cleaned_data)
            formset.save()
            return redirect('ownsets')
        else:
            print(formset.errors)
            return redirect('ownsets')

    else:
        formset = KarteFormSet(instance=stapel)
        return render(request, 'spiel/create.html', {'forms': formset})

@login_required()
def discover(request, mode):
    order = ""
    if mode == "normal":
        order = "-id"
    elif mode == "random":
        order = "?"
    elif mode == "played":
        order = "-Played"
    elif mode == "liked":
        order = "-CountLikedBy"
    elif mode == "followed":
        order = "-CountFollowedBy"
    elif mode == "hot":
        order = "-points"
    try:
        if mode == "hot":
            stapel = Stapel.objects.all().filter(Shared=1, DefaultSet=0, Created__gte=(timezone.now() - timedelta(days=7)))
            mySets = stapel.annotate(number_cards=Count('karte'), points=(F('Played') * 2 + F('CountLikedBy') * 2 + F('CountFollowedBy'))).order_by(order)
        else:
            stapel = Stapel.objects.all().filter(Shared=1, DefaultSet=0)
            mySets = stapel.annotate(number_cards=Count('karte')).order_by(order)
        paginator = Paginator(mySets, 12)
        page = request.GET.get('page') if request.GET.get('page') is not None else 1
        set = paginator.get_page(page)
        args = {"decks": set}
    except exceptions.ObjectDoesNotExist:
        #TODO: Keine Stapel message hinzufügen
        return render(request, 'spiel/ownSets.html')
    return render(request, 'spiel/discover.html', args)


#TODO: Benachrichtigung an admin, wenn Kommentar freigeschaltet werden muss

def kommentare(request):
    """
    Diese View ist für die Kommentare zuständig. Nur eingeloggte Nutzer können Kommentare schreiben.
    :param request:
    :return:
    """
    if request.method == "POST":
        #Überprüfen ob Nutzer eingeloggt ist, sonst darf er keine Kommentare erstellen können
        if request.user.is_authenticated:
            form = KommentarForm(request.POST)
            if form.is_valid():
                result = form.save(commit=False)
                result.Ersteller = request.user
                extractor = urlextract.URLExtract()
                url = extractor.find_urls(form.cleaned_data["Kommentar"])
                if not url:
                    result.save()
                else:
                    result.Freigeschaltet = False
                    result.save()
                return redirect("../../comment/")
            else:
                #TODO: Nutzer/IP bannen; gucken woran Hoeypot gescheitert ist
                pass
    else:
        form = KommentarForm()
    with transaction.atomic():
        comments = Kommentar.objects.all().filter(Freigeschaltet=True)
        result = comments

        #Überprüfen ob Nutzer eingeloggt ist. Wenn True, dann füge seine nicht freigegebenen Kommentare als Schattenkommentare hinzu. Sieht nur er, sonst keiner.
        if request.user.is_authenticated:
            shadow = Kommentar.objects.all().filter(Freigeschaltet=False, Ersteller=request.user)
            result = comments | shadow
    return render(request, 'spiel/kommentare.html', {'form':form, 'comments':result.order_by('-Timestamp')})


def profil_settings(request):
    username_form = SettingUsernameForm()
    email_form = SettingEmailForm()
    password_form = SettingPasswordForm()
    error = None
    success = None
    if request.method == "POST":
        if request.POST.get('username') is not None:
            username_form = SettingUsernameForm(request.POST)
            if username_form.is_valid():
                if request.user.check_password(username_form.cleaned_data['password']):
                    user = request.user
                    user.username = username_form.cleaned_data['username']
                    user.save()
                    success = "Der Benutzername wurde erfolgreich geändert!"
                else:
                    error = "Das Passwort ist falsch!"


        elif request.POST.get('email') is not None:
            if request.POST.get('email') is not None:
                email_form = SettingEmailForm(request.POST)
                if email_form.is_valid():
                    if request.user.check_password(email_form.cleaned_data['password']):
                        user = request.user
                        user.email = email_form.cleaned_data['email']
                        user.save()
                        success = "Die EMail-Adresse wurde erfolgreich geändert!"
                    else:
                        error = "Das Passwort ist falsch!"


        elif request.POST.get('password2') is not None:
            if request.POST.get('password') is not None:
                password_form = SettingPasswordForm(request.POST)
                if password_form.is_valid():
                    if request.user.check_password(password_form.cleaned_data['old_password']):
                        user = request.user
                        print(user)
                        user.set_password(password_form.cleaned_data['password'])
                        user.save()
                        success = "Das Passwort wurde erfolgreich geändert!"
                    else:
                        error = "Das alte Passwort ist falsch!"

    return render(request, 'spiel/profilsettings.html', {'username_form':username_form, 'email_form': email_form,
                                                         'password_form':password_form, 'error':error, 'success':success})