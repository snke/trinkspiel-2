var ids = start_ids;
var rounds = [];
var rule = [];
var answer = [];
var playerNames = names;
var tempName;
var tempName2;
var spieler = 2;
var roundIsNull = false;

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}



$(document).ready(function() {
        if(names.length > 1) {
            tempName = names[Math.floor(Math.random() * names.length)];
            tempName2 = names[Math.floor(Math.random() * names.length)];
            $('#listNames').html("<ul class='list-group'>");
            for(i = 0; i < names.length; i++){
                $('#listNames').append("<li class='list-group-item border-0 d-flex w-100 justify-content-between'>" +names[i] + "<button type='button' class='btn btn-danger' onclick='removePlayer("+i+")'>Löschen</button></li>");
            }
	         $('#listNames').append("</ul>");

            $(document).keypress(function (e) {
                if (e.which == 13 || e.which == 32) {
                    e.preventDefault();
                    if(this.id == "doNothing"){
                        newPlayer();
                    }
                    else if(roundIsNull){
                        elementIsNull();
                    }
                    else {
                        fetch();
                    }
                }
            });

            $("#angepasst").click(function (event) {
                if((event.target.id != "menu")) {
                    if (roundIsNull) {
                        elementIsNull();
                    }
                    else {
                        fetch();
                    }
                }
            });

			//$("body").css('background-color','rgba(0,65,106,0.95)');
            $('#menu').show();
            $("#content").html("Immer wenn ihr eine Aufgabe schafft bei der keine Schluckzahl bei steht, dürft ihr 5 verteilen, sonst müsst ihr 3 trinken!");
        }
    });

function removePlayer(number){
        if(playerNames.length > 2){
            playerNames.splice(number, 1);
            $('#listNames').html("<ul class='list-group'>");
            for(i = 0; i < playerNames.length; i++){
                $('#listNames').append("<li class='list-group-item  border-0 d-flex w-100 justify-content-between'><span>" + playerNames[i] + "</span><button type='button' class='btn btn-danger' onclick='removePlayer("+i+")'>Löschen</button></li>");
                //$('#listNames').append(names[i] + " <span onclick='removePlayer("+i+")'>X</span>| ");
            }
            $('#listNames').append("</ul>");
        }
}

function addNewPlayer() {
    playerNames.push($("#newName").val());
    $("#newName").val("");
    $('#listNames').html("<ul class='list-group'>");
    for(i = 0; i < playerNames.length; i++){
        $('#listNames').append("<li class='list-group-item border-0 d-flex w-100 justify-content-between'>" +playerNames[i] + "<button type='button' class='btn btn-danger' onclick='removePlayer("+i+")'>Löschen</button></li>");
        //$('#listNames').append(names[i] + " <span onclick='removePlayer("+i+")'>X</span>| ");
    }
    $('#listNames').append("</ul>");
}


function replaceName(text,use) {
    if (use === 1) {
        tempName = Math.floor(Math.random() * names.length);
        tempName2 = Math.floor(Math.random() * names.length);
        while (tempName === tempName2) {
        tempName2 = Math.floor(Math.random() * names.length);
        }
    }
    var text = text;
    while(text.indexOf("name1") != -1) {
        text = text.replace('name1', names[tempName]);
    }
    while(text.indexOf("name2") != -1) {
        text = text.replace('name2', names[tempName2]);
    }
    return text;
}

function elementIsNull(){
    for(i = 0; i < rounds.length; i++) {
        if (rounds[i] == 0) {
            rounds.splice(i, 1);
            $('#content').html(answer[i]);
            answer.splice(i, 1);
			rule.splice(i, 1);
			updateRules();
            roundIsNull = false;
            break;
        }
    }
    for(i = 0; i < rounds.length; i++) {
        if(rounds[i] == 0){
           roundIsNull = true;
            break;
        }
        else {
           roundIsNull = false;
        }
    }
}

function updateRules(){
		//$("#rules").html("<ul class='list-group'>");
        $("#rules").html("");
		for(i = 0; i < rule.length; i++){
			$('#rules').append("<p>" + rule[i] + '</p>');//"<li class='list-group-item border-0'>" + rule[i] + "</li>");
		}
		//$('#rules').append("</ul>");
}

function fetch() {
    for(i = 0; i < rounds.length; i++){
        rounds[i] = rounds[i]-1;
        if(rounds[i] == 0){
            roundIsNull = true;
        }
    }
    if(ids.length != 0){
        var temp = Math.floor(Math.random()*ids.length);
        var tempElement = ids[temp];
        ids.splice(temp,1);
        function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
            return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
        }
        var csrftoken = getCookie('csrftoken');
        $.ajax({method: "GET", dataType:"json", url:"../../fetch/", beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
        },
            data: {id  : tempElement}, success: function(result) {
            if(result.content.TaskAnswer == ""){
                $('#content').html(replaceName(result.content.Task,1));
            }
            else {
                rounds.push((Math.floor(Math.random()*7)) + 8);
                $('#content').html(replaceName(result.content.Task,1));
                answer.push(replaceName(result.content.TaskAnswer,2));
				rule.push(replaceName(result.content.Task,2));
				updateRules();
            }
        }});
    }
    else {
        for(i = 0; i < rounds.length; i++){
            rounds.splice(i,1);
            answer.splice(i,1);
        }
        $('#content').html("Ihr habt alle Aufgaben durchgespielt, ihr Alkoholiker! Drückt einfach auf den Bildschirm, wenn ihr von vorne Anfangen möchtest");
        ids = start_ids;
    }

}
