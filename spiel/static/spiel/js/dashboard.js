/*Funktion um cookies nach Namen zu bekommen. Von der Django Dokumentation kopiert*/
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

/*Eventhandler um Stapel auf öffentlich und privat zu stellen*/
$('.shared').change( function() {
    if($(this).prop('checked')) {
        function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
            return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
        }
        var csrftoken = getCookie('csrftoken');
        $.ajax({method: "GET", dataType:"json", url:"../../../shared/", beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
        },
            data: {id  : this.id, status: 1},
            success: function(result) {
            console.log(result)
        }});
    }
    else {
        function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
            return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
        }
        var csrftoken = getCookie('csrftoken');
        $.ajax({method: "GET", dataType:"json", url:"../../../shared/", beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
        },
            data: {id  : this.id, status: 0},
            success: function(result) {
            console.log(result)
        }});
    }
});

/*Funktion um die Hoverfunktion zu aktivieren.
* Diese sorgt dafür, dass die Hintergrundfarbe des Stapelnamens leicht grau wird.*/
function activateHover() {
    $(".deckName").hover(function() {
        $(this).css("background-color", "#E6E6E6");
    }, function () {
        $(this).css("background-color", "white");
    });
}

/*TODO: implement error handling wenn mehr als 50 Zeichen*/
/*Ajaxfunktion um den Stapelnamen zu ändern*/
function changeNameAjax(name, id) {
    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
            return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
        }
        var csrftoken = getCookie('csrftoken');
        $.ajax({
            method: "GET",
            dataType:"json",
            url:"../../../deckname/",
            beforeSend: function(xhr, settings) {
                if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                    xhr.setRequestHeader("X-CSRFToken", csrftoken);
                }
            },
            data: {name: name, id: id},
            success: function(result) {
                    status = result.status;
                    if(status == 200){
                        console.log("success");
                    }
                    else {
                        console.log("error");
                    }
                }
        });
}

/*Aktiviert, dass man decktitle anklicken kann.
* Daraus wird dann ein Eingabefeld um Stapelnamen anzupassen.*/
function activateDeckTitle() {
    $(".deckTitle").click(function() {
        $(".deckTitle").off();
        var name = $(this).text();
        console.log($(this).text());
        $(this).html('<input type="text" class="form-control" id="tempDeck" placeholder="Stapelname" maxlength="50">');
        $("#tempDeck").focus().val(name);
        $(".deckTitle").focusout(function() {
            if($("#tempDeck").val() != ""){
                changeNameAjax($("#tempDeck").val(), this.id);
                $(this).html('<span class="deckName rounded">'+ $("#tempDeck").val()+'</span>');
                activateDeckTitle();
                activateHover();
            }
        });
    });
}

$(".deleteButton").click(function() {
    $(".deleteButtonModal").attr("id", $(this).attr("id"));
});

/*Modal funktion um Stapel zu löschen
* TODO: Error handling implementieren*/
$(".deleteButtonModal").click(function() {
   var id =  $(this).attr("id");

   function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
            return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
        }
        var csrftoken = getCookie('csrftoken');
        $.ajax({
            method: "GET",
            dataType:"text",
            url:"../../deletedeck/",
            beforeSend: function(xhr, settings) {
                if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                    xhr.setRequestHeader("X-CSRFToken", csrftoken);
                }
            },
            data: {id: id},
            success: function() {
                    $('#deleteModal').modal('hide');
                    $('#card_'+id).remove();
                },
            error: function() {
                    console.log("error");
                }
        });
});

activateDeckTitle();
activateHover();
